# coding: utf-8

"""
    FastAPI

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

    The version of the OpenAPI document: 0.1.0
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from openapi_client.api.default_api import DefaultApi


class TestDefaultApi(unittest.TestCase):
    """DefaultApi unit test stubs"""

    def setUp(self) -> None:
        self.api = DefaultApi()

    def tearDown(self) -> None:
        pass

    def test_add_session_event_session_id_events_post(self) -> None:
        """Test case for add_session_event_session_id_events_post

        Add Session Event
        """
        pass

    def test_add_session_profile_session_id_profiles_post(self) -> None:
        """Test case for add_session_profile_session_id_profiles_post

        Add Session Profile
        """
        pass

    def test_add_session_signature_session_id_provenance_post(self) -> None:
        """Test case for add_session_signature_session_id_provenance_post

        Add Session Signature
        """
        pass

    def test_create_data_data_post(self) -> None:
        """Test case for create_data_data_post

        Create Data
        """
        pass

    def test_create_data_user_data_users_post(self) -> None:
        """Test case for create_data_user_data_users_post

        Create Data User
        """
        pass

    def test_create_metadata_metadata_post(self) -> None:
        """Test case for create_metadata_metadata_post

        Create Metadata
        """
        pass

    def test_create_provider_providers_post(self) -> None:
        """Test case for create_provider_providers_post

        Create Provider
        """
        pass

    def test_create_relationship_relationship_post(self) -> None:
        """Test case for create_relationship_relationship_post

        Create Relationship
        """
        pass

    def test_create_session_session_post(self) -> None:
        """Test case for create_session_session_post

        Create Session
        """
        pass

    def test_create_user_users_post(self) -> None:
        """Test case for create_user_users_post

        Create User
        """
        pass

    def test_delete_data_data_delete(self) -> None:
        """Test case for delete_data_data_delete

        Delete Data
        """
        pass

    def test_delete_data_user_data_users_delete(self) -> None:
        """Test case for delete_data_user_data_users_delete

        Delete Data User
        """
        pass

    def test_delete_metadata_metadata_delete(self) -> None:
        """Test case for delete_metadata_metadata_delete

        Delete Metadata
        """
        pass

    def test_delete_providers_providers_delete(self) -> None:
        """Test case for delete_providers_providers_delete

        Delete Providers
        """
        pass

    def test_delete_relationship_relationship_delete(self) -> None:
        """Test case for delete_relationship_relationship_delete

        Delete Relationship
        """
        pass

    def test_delete_session_session_delete(self) -> None:
        """Test case for delete_session_session_delete

        Delete Session
        """
        pass

    def test_delete_users_users_delete(self) -> None:
        """Test case for delete_users_users_delete

        Delete Users
        """
        pass

    def test_list_data_data_get(self) -> None:
        """Test case for list_data_data_get

        List Data
        """
        pass

    def test_list_data_users_data_users_get(self) -> None:
        """Test case for list_data_users_data_users_get

        List Data Users
        """
        pass

    def test_list_metadata_metadata_get(self) -> None:
        """Test case for list_metadata_metadata_get

        List Metadata
        """
        pass

    def test_list_providers_providers_get(self) -> None:
        """Test case for list_providers_providers_get

        List Providers
        """
        pass

    def test_list_relationship_relationship_get(self) -> None:
        """Test case for list_relationship_relationship_get

        List Relationship
        """
        pass

    def test_list_session_session_get(self) -> None:
        """Test case for list_session_session_get

        List Session
        """
        pass

    def test_list_users_users_get(self) -> None:
        """Test case for list_users_users_get

        List Users
        """
        pass

    def test_read_current_user_users_me_get(self) -> None:
        """Test case for read_current_user_users_me_get

        Read Current User
        """
        pass

    def test_update_data_data_id_patch(self) -> None:
        """Test case for update_data_data_id_patch

        Update Data
        """
        pass

    def test_update_data_user_data_users_id_patch(self) -> None:
        """Test case for update_data_user_data_users_id_patch

        Update Data User
        """
        pass

    def test_update_metadata_metadata_id_patch(self) -> None:
        """Test case for update_metadata_metadata_id_patch

        Update Metadata
        """
        pass

    def test_update_relationship_relationship_id_patch(self) -> None:
        """Test case for update_relationship_relationship_id_patch

        Update Relationship
        """
        pass

    def test_update_session_events_session_id_events_patch(self) -> None:
        """Test case for update_session_events_session_id_events_patch

        Update Session Events
        """
        pass

    def test_update_session_profile_session_id_profiles_patch(self) -> None:
        """Test case for update_session_profile_session_id_profiles_patch

        Update Session Profile
        """
        pass

    def test_update_session_provenance_session_id_provenance_patch(self) -> None:
        """Test case for update_session_provenance_session_id_provenance_patch

        Update Session Provenance
        """
        pass

    def test_update_session_session_id_patch(self) -> None:
        """Test case for update_session_session_id_patch

        Update Session
        """
        pass

    def test_update_user_users_id_patch(self) -> None:
        """Test case for update_user_users_id_patch

        Update User
        """
        pass


if __name__ == '__main__':
    unittest.main()
