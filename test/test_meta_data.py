# coding: utf-8

"""
    FastAPI

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

    The version of the OpenAPI document: 0.1.0
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from openapi_client.models.meta_data import MetaData

class TestMetaData(unittest.TestCase):
    """MetaData unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional) -> MetaData:
        """Test MetaData
            include_optional is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `MetaData`
        """
        model = MetaData()
        if include_optional:
            return MetaData(
                id = '5eb7cf5a86d9755df3a6c593',
                provider_md_id = '',
                provider_id = '5eb7cf5a86d9755df3a6c593',
                content = '',
                content_type = 'text',
                relationships = [
                    '5eb7cf5a86d9755df3a6c593'
                    ],
                data = [
                    '5eb7cf5a86d9755df3a6c593'
                    ],
                access = 'public',
                unique_name = '',
                data_users = [
                    '5eb7cf5a86d9755df3a6c593'
                    ],
                sessions = [
                    '5eb7cf5a86d9755df3a6c593'
                    ],
                create_ts = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'),
                modify_ts = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f')
            )
        else:
            return MetaData(
                id = '5eb7cf5a86d9755df3a6c593',
                provider_md_id = '',
                provider_id = '5eb7cf5a86d9755df3a6c593',
                content = '',
                unique_name = '',
                create_ts = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'),
                modify_ts = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'),
        )
        """

    def testMetaData(self):
        """Test MetaData"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
