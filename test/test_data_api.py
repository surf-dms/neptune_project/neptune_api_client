# coding: utf-8

"""
    FastAPI

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

    The version of the OpenAPI document: 0.1.0
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from openapi_client.api.data_api import DataApi


class TestDataApi(unittest.TestCase):
    """DataApi unit test stubs"""

    def setUp(self) -> None:
        self.api = DataApi()

    def tearDown(self) -> None:
        pass

    def test_create_data_data_post(self) -> None:
        """Test case for create_data_data_post

        Create Data
        """
        pass

    def test_delete_data_data_delete(self) -> None:
        """Test case for delete_data_data_delete

        Delete Data
        """
        pass

    def test_list_data_data_get(self) -> None:
        """Test case for list_data_data_get

        List Data
        """
        pass

    def test_update_data_data_id_patch(self) -> None:
        """Test case for update_data_data_id_patch

        Update Data
        """
        pass


if __name__ == '__main__':
    unittest.main()
