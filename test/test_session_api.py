# coding: utf-8

"""
    FastAPI

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

    The version of the OpenAPI document: 0.1.0
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from openapi_client.api.session_api import SessionApi


class TestSessionApi(unittest.TestCase):
    """SessionApi unit test stubs"""

    def setUp(self) -> None:
        self.api = SessionApi()

    def tearDown(self) -> None:
        pass

    def test_add_session_event_session_id_events_post(self) -> None:
        """Test case for add_session_event_session_id_events_post

        Add Session Event
        """
        pass

    def test_add_session_profile_session_id_profiles_post(self) -> None:
        """Test case for add_session_profile_session_id_profiles_post

        Add Session Profile
        """
        pass

    def test_add_session_signature_session_id_provenance_post(self) -> None:
        """Test case for add_session_signature_session_id_provenance_post

        Add Session Signature
        """
        pass

    def test_create_session_session_post(self) -> None:
        """Test case for create_session_session_post

        Create Session
        """
        pass

    def test_delete_session_session_delete(self) -> None:
        """Test case for delete_session_session_delete

        Delete Session
        """
        pass

    def test_list_session_session_get(self) -> None:
        """Test case for list_session_session_get

        List Session
        """
        pass

    def test_update_session_events_session_id_events_patch(self) -> None:
        """Test case for update_session_events_session_id_events_patch

        Update Session Events
        """
        pass

    def test_update_session_profile_session_id_profiles_patch(self) -> None:
        """Test case for update_session_profile_session_id_profiles_patch

        Update Session Profile
        """
        pass

    def test_update_session_provenance_session_id_provenance_patch(self) -> None:
        """Test case for update_session_provenance_session_id_provenance_patch

        Update Session Provenance
        """
        pass

    def test_update_session_session_id_patch(self) -> None:
        """Test case for update_session_session_id_patch

        Update Session
        """
        pass


if __name__ == '__main__':
    unittest.main()
