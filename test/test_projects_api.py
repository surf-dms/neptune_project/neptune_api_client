# coding: utf-8

"""
    FastAPI

    No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)

    The version of the OpenAPI document: 0.1.0
    Generated by OpenAPI Generator (https://openapi-generator.tech)

    Do not edit the class manually.
"""  # noqa: E501


import unittest

from openapi_client.api.projects_api import ProjectsApi


class TestProjectsApi(unittest.TestCase):
    """ProjectsApi unit test stubs"""

    def setUp(self) -> None:
        self.api = ProjectsApi()

    def tearDown(self) -> None:
        pass

    def test_create_project_projects_post(self) -> None:
        """Test case for create_project_projects_post

        Create Project
        """
        pass

    def test_delete_project_sram_user_projects_name_sram_email_delete(self) -> None:
        """Test case for delete_project_sram_user_projects_name_sram_email_delete

        Delete Project Sram User
        """
        pass

    def test_delete_projects_projects_delete(self) -> None:
        """Test case for delete_projects_projects_delete

        Delete Projects
        """
        pass

    def test_list_projects_projects_get(self) -> None:
        """Test case for list_projects_projects_get

        List Projects
        """
        pass

    def test_register_project_sram_user_projects_name_sram_email_post(self) -> None:
        """Test case for register_project_sram_user_projects_name_sram_email_post

        Register Project Sram User
        """
        pass

    def test_update_project_projects_id_patch(self) -> None:
        """Test case for update_project_projects_id_patch

        Update Project
        """
        pass

    def test_verify_project_sram_user_registration_projects_name_sram_email_get(self) -> None:
        """Test case for verify_project_sram_user_registration_projects_name_sram_email_get

        Verify Project Sram User Registration
        """
        pass


if __name__ == '__main__':
    unittest.main()
