# DataUserCreate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**username** | **str** |  | 
**username_source** | **str** |  | 
**provider_id** | **str** |  | 
**user_id** | **List[str]** |  | [optional] [default to []]
**relationships** | **List[str]** |  | [optional] [default to []]
**password** | **str** |  | [optional] 
**role** | **str** |  | [optional] [default to 'user']
**public_key** | **str** |  | [optional] 

## Example

```python
from openapi_client.models.data_user_create import DataUserCreate

# TODO update the JSON string below
json = "{}"
# create an instance of DataUserCreate from a JSON string
data_user_create_instance = DataUserCreate.from_json(json)
# print the JSON string representation of the object
print(DataUserCreate.to_json())

# convert the object into a dict
data_user_create_dict = data_user_create_instance.to_dict()
# create an instance of DataUserCreate from a dict
data_user_create_from_dict = DataUserCreate.from_dict(data_user_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


