# openapi_client.RelationshipApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_relationship_relationship_post**](RelationshipApi.md#create_relationship_relationship_post) | **POST** /relationship/ | Create Relationship
[**delete_relationship_relationship_delete**](RelationshipApi.md#delete_relationship_relationship_delete) | **DELETE** /relationship/ | Delete Relationship
[**list_relationship_relationship_get**](RelationshipApi.md#list_relationship_relationship_get) | **GET** /relationship/ | List Relationship
[**update_relationship_relationship_id_patch**](RelationshipApi.md#update_relationship_relationship_id_patch) | **PATCH** /relationship/{id} | Update Relationship


# **create_relationship_relationship_post**
> Relationship create_relationship_relationship_post(relationship_create)

Create Relationship

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.relationship import Relationship
from openapi_client.models.relationship_create import RelationshipCreate
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.RelationshipApi(api_client)
    relationship_create = openapi_client.RelationshipCreate() # RelationshipCreate | 

    try:
        # Create Relationship
        api_response = api_instance.create_relationship_relationship_post(relationship_create)
        print("The response of RelationshipApi->create_relationship_relationship_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RelationshipApi->create_relationship_relationship_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **relationship_create** | [**RelationshipCreate**](RelationshipCreate.md)|  | 

### Return type

[**Relationship**](Relationship.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_relationship_relationship_delete**
> object delete_relationship_relationship_delete(id=id)

Delete Relationship

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.RelationshipApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str |  (optional)

    try:
        # Delete Relationship
        api_response = api_instance.delete_relationship_relationship_delete(id=id)
        print("The response of RelationshipApi->delete_relationship_relationship_delete:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RelationshipApi->delete_relationship_relationship_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | [optional] 

### Return type

**object**

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_relationship_relationship_get**
> List[Relationship] list_relationship_relationship_get(data_id=data_id, data_user=data_user)

List Relationship

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.relationship import Relationship
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.RelationshipApi(api_client)
    data_id = '5eb7cf5a86d9755df3a6c593' # str |  (optional)
    data_user = '5eb7cf5a86d9755df3a6c593' # str |  (optional)

    try:
        # List Relationship
        api_response = api_instance.list_relationship_relationship_get(data_id=data_id, data_user=data_user)
        print("The response of RelationshipApi->list_relationship_relationship_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RelationshipApi->list_relationship_relationship_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data_id** | **str**|  | [optional] 
 **data_user** | **str**|  | [optional] 

### Return type

[**List[Relationship]**](Relationship.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_relationship_relationship_id_patch**
> Relationship update_relationship_relationship_id_patch(id, relationship_create)

Update Relationship

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.relationship import Relationship
from openapi_client.models.relationship_create import RelationshipCreate
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.RelationshipApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str | 
    relationship_create = openapi_client.RelationshipCreate() # RelationshipCreate | 

    try:
        # Update Relationship
        api_response = api_instance.update_relationship_relationship_id_patch(id, relationship_create)
        print("The response of RelationshipApi->update_relationship_relationship_id_patch:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling RelationshipApi->update_relationship_relationship_id_patch: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **relationship_create** | [**RelationshipCreate**](RelationshipCreate.md)|  | 

### Return type

[**Relationship**](Relationship.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

