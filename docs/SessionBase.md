# SessionBase


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**events** | [**List[Event]**](Event.md) |  | [optional] [default to []]
**lifetime** | **int** |  | [optional] [default to 3600]
**access** | **str** |  | [optional] [default to 'private']
**participants** | **List[str]** |  | [optional] [default to []]
**profiles** | [**List[Profile]**](Profile.md) |  | [optional] [default to []]
**provenance** | **List[str]** |  | [optional] [default to []]

## Example

```python
from openapi_client.models.session_base import SessionBase

# TODO update the JSON string below
json = "{}"
# create an instance of SessionBase from a JSON string
session_base_instance = SessionBase.from_json(json)
# print the JSON string representation of the object
print(SessionBase.to_json())

# convert the object into a dict
session_base_dict = session_base_instance.to_dict()
# create an instance of SessionBase from a dict
session_base_from_dict = SessionBase.from_dict(session_base_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


