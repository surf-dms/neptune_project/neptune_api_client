# Hostname


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from openapi_client.models.hostname import Hostname

# TODO update the JSON string below
json = "{}"
# create an instance of Hostname from a JSON string
hostname_instance = Hostname.from_json(json)
# print the JSON string representation of the object
print(Hostname.to_json())

# convert the object into a dict
hostname_dict = hostname_instance.to_dict()
# create an instance of Hostname from a dict
hostname_form_dict = hostname.from_dict(hostname_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


