# ProvenanceActionType


## Enum

* `APPROVED` (value: `'approved'`)

* `REJECTED` (value: `'rejected'`)

* `SHARED` (value: `'shared'`)

* `COPIED` (value: `'copied'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


