# EpType


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from openapi_client.models.ep_type import EpType

# TODO update the JSON string below
json = "{}"
# create an instance of EpType from a JSON string
ep_type_instance = EpType.from_json(json)
# print the JSON string representation of the object
print(EpType.to_json())

# convert the object into a dict
ep_type_dict = ep_type_instance.to_dict()
# create an instance of EpType from a dict
ep_type_form_dict = ep_type.from_dict(ep_type_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


