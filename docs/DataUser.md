# DataUser


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**username** | **str** |  | 
**username_source** | **str** |  | 
**provider_id** | **str** |  | 
**user_id** | **List[str]** |  | [optional] [default to []]
**relationships** | **List[str]** |  | [optional] [default to []]
**password** | **str** |  | [optional] 
**role** | **str** |  | [optional] [default to 'user']
**public_key** | **str** |  | [optional] 
**create_ts** | **datetime** |  | 
**modify_ts** | **datetime** |  | 

## Example

```python
from openapi_client.models.data_user import DataUser

# TODO update the JSON string below
json = "{}"
# create an instance of DataUser from a JSON string
data_user_instance = DataUser.from_json(json)
# print the JSON string representation of the object
print(DataUser.to_json())

# convert the object into a dict
data_user_dict = data_user_instance.to_dict()
# create an instance of DataUser from a dict
data_user_from_dict = DataUser.from_dict(data_user_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


