# DataCreate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**key_in_provider** | **str** |  | 
**provider_id** | **str** |  | 
**friendly_name** | **str** |  | 
**submitted_usernames** | **List[str]** |  | [optional] [default to []]
**relationships** | **List[str]** |  | [optional] [default to []]
**access** | **str** |  | 

## Example

```python
from openapi_client.models.data_create import DataCreate

# TODO update the JSON string below
json = "{}"
# create an instance of DataCreate from a JSON string
data_create_instance = DataCreate.from_json(json)
# print the JSON string representation of the object
print(DataCreate.to_json())

# convert the object into a dict
data_create_dict = data_create_instance.to_dict()
# create an instance of DataCreate from a dict
data_create_from_dict = DataCreate.from_dict(data_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


