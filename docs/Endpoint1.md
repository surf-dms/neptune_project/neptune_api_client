# Endpoint1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hostname** | **str** |  | 
**port** | **int** |  | 
**protocol** | **str** |  | 
**ep_type** | **str** |  | [optional] [default to 'irods']
**zone** | **str** |  | 

## Example

```python
from openapi_client.models.endpoint1 import Endpoint1

# TODO update the JSON string below
json = "{}"
# create an instance of Endpoint1 from a JSON string
endpoint1_instance = Endpoint1.from_json(json)
# print the JSON string representation of the object
print(Endpoint1.to_json())

# convert the object into a dict
endpoint1_dict = endpoint1_instance.to_dict()
# create an instance of Endpoint1 from a dict
endpoint1_from_dict = Endpoint1.from_dict(endpoint1_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


