# ProviderCreate


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**friendly_name** | **str** |  | 
**endpoints** | [**List[ProviderEndpointsInner]**](ProviderEndpointsInner.md) |  | [optional] [default to []]
**endpoint_default_type** | **str** |  | [optional] [default to 'unknown']
**administrators** | **List[str]** |  | [optional] [default to []]

## Example

```python
from openapi_client.models.provider_create import ProviderCreate

# TODO update the JSON string below
json = "{}"
# create an instance of ProviderCreate from a JSON string
provider_create_instance = ProviderCreate.from_json(json)
# print the JSON string representation of the object
print(ProviderCreate.to_json())

# convert the object into a dict
provider_create_dict = provider_create_instance.to_dict()
# create an instance of ProviderCreate from a dict
provider_create_from_dict = ProviderCreate.from_dict(provider_create_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


