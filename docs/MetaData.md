# MetaData


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**provider_md_id** | **str** |  | 
**provider_id** | **str** |  | 
**content** | **str** |  | 
**content_type** | **str** |  | [optional] [default to 'text']
**relationships** | **List[str]** |  | [optional] [default to []]
**data** | **List[str]** |  | [optional] [default to []]
**access** | **str** |  | [optional] [default to 'public']
**unique_name** | **str** |  | 
**data_users** | **List[str]** |  | [optional] [default to []]
**sessions** | **List[str]** |  | [optional] [default to []]
**create_ts** | **datetime** |  | 
**modify_ts** | **datetime** |  | 

## Example

```python
from openapi_client.models.meta_data import MetaData

# TODO update the JSON string below
json = "{}"
# create an instance of MetaData from a JSON string
meta_data_instance = MetaData.from_json(json)
# print the JSON string representation of the object
print(MetaData.to_json())

# convert the object into a dict
meta_data_dict = meta_data_instance.to_dict()
# create an instance of MetaData from a dict
meta_data_from_dict = MetaData.from_dict(meta_data_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


