# Usernames


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from openapi_client.models.usernames import Usernames

# TODO update the JSON string below
json = "{}"
# create an instance of Usernames from a JSON string
usernames_instance = Usernames.from_json(json)
# print the JSON string representation of the object
print(Usernames.to_json())

# convert the object into a dict
usernames_dict = usernames_instance.to_dict()
# create an instance of Usernames from a dict
usernames_form_dict = usernames.from_dict(usernames_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


