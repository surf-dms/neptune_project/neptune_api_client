# ProviderEndpointsInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hostname** | **str** |  | 
**port** | **int** |  | 
**protocol** | **str** |  | 
**ep_type** | **str** |  | [optional] [default to 'irods']
**zone** | **str** |  | 

## Example

```python
from openapi_client.models.provider_endpoints_inner import ProviderEndpointsInner

# TODO update the JSON string below
json = "{}"
# create an instance of ProviderEndpointsInner from a JSON string
provider_endpoints_inner_instance = ProviderEndpointsInner.from_json(json)
# print the JSON string representation of the object
print(ProviderEndpointsInner.to_json())

# convert the object into a dict
provider_endpoints_inner_dict = provider_endpoints_inner_instance.to_dict()
# create an instance of ProviderEndpointsInner from a dict
provider_endpoints_inner_from_dict = ProviderEndpointsInner.from_dict(provider_endpoints_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


