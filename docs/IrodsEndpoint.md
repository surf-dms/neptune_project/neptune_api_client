# IrodsEndpoint


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hostname** | **str** |  | 
**port** | **int** |  | 
**protocol** | **str** |  | 
**ep_type** | **str** |  | [optional] [default to 'irods']
**zone** | **str** |  | 

## Example

```python
from openapi_client.models.irods_endpoint import IrodsEndpoint

# TODO update the JSON string below
json = "{}"
# create an instance of IrodsEndpoint from a JSON string
irods_endpoint_instance = IrodsEndpoint.from_json(json)
# print the JSON string representation of the object
print(IrodsEndpoint.to_json())

# convert the object into a dict
irods_endpoint_dict = irods_endpoint_instance.to_dict()
# create an instance of IrodsEndpoint from a dict
irods_endpoint_from_dict = IrodsEndpoint.from_dict(irods_endpoint_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


