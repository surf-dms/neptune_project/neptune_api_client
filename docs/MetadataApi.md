# openapi_client.MetadataApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_metadata_metadata_post**](MetadataApi.md#create_metadata_metadata_post) | **POST** /metadata/ | Create Metadata
[**delete_metadata_metadata_delete**](MetadataApi.md#delete_metadata_metadata_delete) | **DELETE** /metadata/ | Delete Metadata
[**list_metadata_metadata_get**](MetadataApi.md#list_metadata_metadata_get) | **GET** /metadata/ | List Metadata
[**update_metadata_metadata_id_patch**](MetadataApi.md#update_metadata_metadata_id_patch) | **PATCH** /metadata/{id} | Update Metadata


# **create_metadata_metadata_post**
> MetaData create_metadata_metadata_post(meta_data_base)

Create Metadata

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.meta_data import MetaData
from openapi_client.models.meta_data_base import MetaDataBase
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.MetadataApi(api_client)
    meta_data_base = openapi_client.MetaDataBase() # MetaDataBase | 

    try:
        # Create Metadata
        api_response = api_instance.create_metadata_metadata_post(meta_data_base)
        print("The response of MetadataApi->create_metadata_metadata_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MetadataApi->create_metadata_metadata_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **meta_data_base** | [**MetaDataBase**](MetaDataBase.md)|  | 

### Return type

[**MetaData**](MetaData.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_metadata_metadata_delete**
> object delete_metadata_metadata_delete(id=id)

Delete Metadata

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.MetadataApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str |  (optional)

    try:
        # Delete Metadata
        api_response = api_instance.delete_metadata_metadata_delete(id=id)
        print("The response of MetadataApi->delete_metadata_metadata_delete:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MetadataApi->delete_metadata_metadata_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | [optional] 

### Return type

**object**

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_metadata_metadata_get**
> List[MetaData] list_metadata_metadata_get(unique_name=unique_name, provider_md_id=provider_md_id, provider_id=provider_id, data_users=data_users)

List Metadata

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.meta_data import MetaData
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.MetadataApi(api_client)
    unique_name = 'unique_name_example' # str |  (optional)
    provider_md_id = 'provider_md_id_example' # str |  (optional)
    provider_id = '5eb7cf5a86d9755df3a6c593' # str |  (optional)
    data_users = ['data_users_example'] # List[str] |  (optional)

    try:
        # List Metadata
        api_response = api_instance.list_metadata_metadata_get(unique_name=unique_name, provider_md_id=provider_md_id, provider_id=provider_id, data_users=data_users)
        print("The response of MetadataApi->list_metadata_metadata_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MetadataApi->list_metadata_metadata_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **unique_name** | **str**|  | [optional] 
 **provider_md_id** | **str**|  | [optional] 
 **provider_id** | **str**|  | [optional] 
 **data_users** | [**List[str]**](str.md)|  | [optional] 

### Return type

[**List[MetaData]**](MetaData.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_metadata_metadata_id_patch**
> MetaData update_metadata_metadata_id_patch(id, meta_data_base)

Update Metadata

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.meta_data import MetaData
from openapi_client.models.meta_data_base import MetaDataBase
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.MetadataApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str | 
    meta_data_base = openapi_client.MetaDataBase() # MetaDataBase | 

    try:
        # Update Metadata
        api_response = api_instance.update_metadata_metadata_id_patch(id, meta_data_base)
        print("The response of MetadataApi->update_metadata_metadata_id_patch:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling MetadataApi->update_metadata_metadata_id_patch: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **meta_data_base** | [**MetaDataBase**](MetaDataBase.md)|  | 

### Return type

[**MetaData**](MetaData.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

