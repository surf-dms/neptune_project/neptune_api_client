# DataId


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from openapi_client.models.data_id import DataId

# TODO update the JSON string below
json = "{}"
# create an instance of DataId from a JSON string
data_id_instance = DataId.from_json(json)
# print the JSON string representation of the object
print(DataId.to_json())

# convert the object into a dict
data_id_dict = data_id_instance.to_dict()
# create an instance of DataId from a dict
data_id_form_dict = data_id.from_dict(data_id_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


