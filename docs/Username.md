# Username


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from openapi_client.models.username import Username

# TODO update the JSON string below
json = "{}"
# create an instance of Username from a JSON string
username_instance = Username.from_json(json)
# print the JSON string representation of the object
print(Username.to_json())

# convert the object into a dict
username_dict = username_instance.to_dict()
# create an instance of Username from a dict
username_form_dict = username.from_dict(username_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


