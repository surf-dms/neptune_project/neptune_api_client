# ProviderMdId


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from openapi_client.models.provider_md_id import ProviderMdId

# TODO update the JSON string below
json = "{}"
# create an instance of ProviderMdId from a JSON string
provider_md_id_instance = ProviderMdId.from_json(json)
# print the JSON string representation of the object
print(ProviderMdId.to_json())

# convert the object into a dict
provider_md_id_dict = provider_md_id_instance.to_dict()
# create an instance of ProviderMdId from a dict
provider_md_id_form_dict = provider_md_id.from_dict(provider_md_id_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


