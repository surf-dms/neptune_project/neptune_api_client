# KeyInProvider


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from openapi_client.models.key_in_provider import KeyInProvider

# TODO update the JSON string below
json = "{}"
# create an instance of KeyInProvider from a JSON string
key_in_provider_instance = KeyInProvider.from_json(json)
# print the JSON string representation of the object
print(KeyInProvider.to_json())

# convert the object into a dict
key_in_provider_dict = key_in_provider_instance.to_dict()
# create an instance of KeyInProvider from a dict
key_in_provider_form_dict = key_in_provider.from_dict(key_in_provider_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


