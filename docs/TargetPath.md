# TargetPath


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from openapi_client.models.target_path import TargetPath

# TODO update the JSON string below
json = "{}"
# create an instance of TargetPath from a JSON string
target_path_instance = TargetPath.from_json(json)
# print the JSON string representation of the object
print(TargetPath.to_json())

# convert the object into a dict
target_path_dict = target_path_instance.to_dict()
# create an instance of TargetPath from a dict
target_path_form_dict = target_path.from_dict(target_path_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


