# openapi_client.SessionApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_session_event_session_id_events_post**](SessionApi.md#add_session_event_session_id_events_post) | **POST** /session/{id}/events | Add Session Event
[**add_session_profile_session_id_profiles_post**](SessionApi.md#add_session_profile_session_id_profiles_post) | **POST** /session/{id}/profiles | Add Session Profile
[**add_session_signature_session_id_provenance_post**](SessionApi.md#add_session_signature_session_id_provenance_post) | **POST** /session/{id}/provenance | Add Session Signature
[**create_session_session_post**](SessionApi.md#create_session_session_post) | **POST** /session/ | Create Session
[**delete_session_session_delete**](SessionApi.md#delete_session_session_delete) | **DELETE** /session/ | Delete Session
[**list_session_session_get**](SessionApi.md#list_session_session_get) | **GET** /session/ | List Session
[**update_session_events_session_id_events_patch**](SessionApi.md#update_session_events_session_id_events_patch) | **PATCH** /session/{id}/events | Update Session Events
[**update_session_profile_session_id_profiles_patch**](SessionApi.md#update_session_profile_session_id_profiles_patch) | **PATCH** /session/{id}/profiles | Update Session Profile
[**update_session_provenance_session_id_provenance_patch**](SessionApi.md#update_session_provenance_session_id_provenance_patch) | **PATCH** /session/{id}/provenance | Update Session Provenance
[**update_session_session_id_patch**](SessionApi.md#update_session_session_id_patch) | **PATCH** /session/{id} | Update Session


# **add_session_event_session_id_events_post**
> Session add_session_event_session_id_events_post(id, event)

Add Session Event

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.event import Event
from openapi_client.models.session import Session
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.SessionApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str | 
    event = openapi_client.Event() # Event | 

    try:
        # Add Session Event
        api_response = api_instance.add_session_event_session_id_events_post(id, event)
        print("The response of SessionApi->add_session_event_session_id_events_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionApi->add_session_event_session_id_events_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **event** | [**Event**](Event.md)|  | 

### Return type

[**Session**](Session.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_session_profile_session_id_profiles_post**
> Session add_session_profile_session_id_profiles_post(id, profile)

Add Session Profile

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.profile import Profile
from openapi_client.models.session import Session
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.SessionApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str | 
    profile = openapi_client.Profile() # Profile | 

    try:
        # Add Session Profile
        api_response = api_instance.add_session_profile_session_id_profiles_post(id, profile)
        print("The response of SessionApi->add_session_profile_session_id_profiles_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionApi->add_session_profile_session_id_profiles_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **profile** | [**Profile**](Profile.md)|  | 

### Return type

[**Session**](Session.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **add_session_signature_session_id_provenance_post**
> Session add_session_signature_session_id_provenance_post(id, action, related_session=related_session)

Add Session Signature

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.provenance_action_type import ProvenanceActionType
from openapi_client.models.session import Session
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.SessionApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str | 
    action = openapi_client.ProvenanceActionType() # ProvenanceActionType | 
    related_session = 'related_session_example' # str |  (optional)

    try:
        # Add Session Signature
        api_response = api_instance.add_session_signature_session_id_provenance_post(id, action, related_session=related_session)
        print("The response of SessionApi->add_session_signature_session_id_provenance_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionApi->add_session_signature_session_id_provenance_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **action** | [**ProvenanceActionType**](.md)|  | 
 **related_session** | **str**|  | [optional] 

### Return type

[**Session**](Session.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_session_session_post**
> Session create_session_session_post(session_base)

Create Session

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.session import Session
from openapi_client.models.session_base import SessionBase
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.SessionApi(api_client)
    session_base = openapi_client.SessionBase() # SessionBase | 

    try:
        # Create Session
        api_response = api_instance.create_session_session_post(session_base)
        print("The response of SessionApi->create_session_session_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionApi->create_session_session_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_base** | [**SessionBase**](SessionBase.md)|  | 

### Return type

[**Session**](Session.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_session_session_delete**
> object delete_session_session_delete(id=id)

Delete Session

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.SessionApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str |  (optional)

    try:
        # Delete Session
        api_response = api_instance.delete_session_session_delete(id=id)
        print("The response of SessionApi->delete_session_session_delete:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionApi->delete_session_session_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | [optional] 

### Return type

**object**

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_session_session_get**
> List[Session] list_session_session_get(code=code, status=status, access=access, time_interval=time_interval, author=author, text=text, json_search=json_search)

List Session

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.session import Session
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.SessionApi(api_client)
    code = 'code_example' # str |  (optional)
    status = 'status_example' # str |  (optional)
    access = 'access_example' # str |  (optional)
    time_interval = 'time_interval_example' # str |  (optional)
    author = 'author_example' # str |  (optional)
    text = 'text_example' # str |  (optional)
    json_search = 'json_search_example' # str |  (optional)

    try:
        # List Session
        api_response = api_instance.list_session_session_get(code=code, status=status, access=access, time_interval=time_interval, author=author, text=text, json_search=json_search)
        print("The response of SessionApi->list_session_session_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionApi->list_session_session_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **str**|  | [optional] 
 **status** | **str**|  | [optional] 
 **access** | **str**|  | [optional] 
 **time_interval** | **str**|  | [optional] 
 **author** | **str**|  | [optional] 
 **text** | **str**|  | [optional] 
 **json_search** | **str**|  | [optional] 

### Return type

[**List[Session]**](Session.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_session_events_session_id_events_patch**
> Session update_session_events_session_id_events_patch(id, event)

Update Session Events

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.event import Event
from openapi_client.models.session import Session
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.SessionApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str | 
    event = [openapi_client.Event()] # List[Event] | 

    try:
        # Update Session Events
        api_response = api_instance.update_session_events_session_id_events_patch(id, event)
        print("The response of SessionApi->update_session_events_session_id_events_patch:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionApi->update_session_events_session_id_events_patch: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **event** | [**List[Event]**](Event.md)|  | 

### Return type

[**Session**](Session.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_session_profile_session_id_profiles_patch**
> Session update_session_profile_session_id_profiles_patch(id, profile)

Update Session Profile

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.profile import Profile
from openapi_client.models.session import Session
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.SessionApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str | 
    profile = [openapi_client.Profile()] # List[Profile] | 

    try:
        # Update Session Profile
        api_response = api_instance.update_session_profile_session_id_profiles_patch(id, profile)
        print("The response of SessionApi->update_session_profile_session_id_profiles_patch:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionApi->update_session_profile_session_id_profiles_patch: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **profile** | [**List[Profile]**](Profile.md)|  | 

### Return type

[**Session**](Session.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_session_provenance_session_id_provenance_patch**
> Session update_session_provenance_session_id_provenance_patch(id, request_body)

Update Session Provenance

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.session import Session
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.SessionApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str | 
    request_body = ['request_body_example'] # List[Optional[str]] | 

    try:
        # Update Session Provenance
        api_response = api_instance.update_session_provenance_session_id_provenance_patch(id, request_body)
        print("The response of SessionApi->update_session_provenance_session_id_provenance_patch:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionApi->update_session_provenance_session_id_provenance_patch: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **request_body** | [**List[Optional[str]]**](str.md)|  | 

### Return type

[**Session**](Session.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_session_session_id_patch**
> Session update_session_session_id_patch(id, session_base)

Update Session

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.session import Session
from openapi_client.models.session_base import SessionBase
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.SessionApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str | 
    session_base = openapi_client.SessionBase() # SessionBase | 

    try:
        # Update Session
        api_response = api_instance.update_session_session_id_patch(id, session_base)
        print("The response of SessionApi->update_session_session_id_patch:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling SessionApi->update_session_session_id_patch: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **session_base** | [**SessionBase**](SessionBase.md)|  | 

### Return type

[**Session**](Session.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

