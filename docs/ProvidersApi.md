# openapi_client.ProvidersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_provider_providers_post**](ProvidersApi.md#create_provider_providers_post) | **POST** /providers/ | Create Provider
[**delete_providers_providers_delete**](ProvidersApi.md#delete_providers_providers_delete) | **DELETE** /providers/ | Delete Providers
[**list_providers_providers_get**](ProvidersApi.md#list_providers_providers_get) | **GET** /providers/ | List Providers


# **create_provider_providers_post**
> Provider create_provider_providers_post(provider_create)

Create Provider

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.provider import Provider
from openapi_client.models.provider_create import ProviderCreate
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.ProvidersApi(api_client)
    provider_create = openapi_client.ProviderCreate() # ProviderCreate | 

    try:
        # Create Provider
        api_response = api_instance.create_provider_providers_post(provider_create)
        print("The response of ProvidersApi->create_provider_providers_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ProvidersApi->create_provider_providers_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider_create** | [**ProviderCreate**](ProviderCreate.md)|  | 

### Return type

[**Provider**](Provider.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_providers_providers_delete**
> object delete_providers_providers_delete(id=id)

Delete Providers

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.ProvidersApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str |  (optional)

    try:
        # Delete Providers
        api_response = api_instance.delete_providers_providers_delete(id=id)
        print("The response of ProvidersApi->delete_providers_providers_delete:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ProvidersApi->delete_providers_providers_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | [optional] 

### Return type

**object**

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_providers_providers_get**
> List[Provider] list_providers_providers_get(hostname=hostname, port=port, protocol=protocol, ep_type=ep_type, friendly_name=friendly_name)

List Providers

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.provider import Provider
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.ProvidersApi(api_client)
    hostname = 'hostname_example' # str |  (optional)
    port = 56 # int |  (optional)
    protocol = 'protocol_example' # str |  (optional)
    ep_type = 'ep_type_example' # str |  (optional)
    friendly_name = 'friendly_name_example' # str |  (optional)

    try:
        # List Providers
        api_response = api_instance.list_providers_providers_get(hostname=hostname, port=port, protocol=protocol, ep_type=ep_type, friendly_name=friendly_name)
        print("The response of ProvidersApi->list_providers_providers_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling ProvidersApi->list_providers_providers_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **hostname** | **str**|  | [optional] 
 **port** | **int**|  | [optional] 
 **protocol** | **str**|  | [optional] 
 **ep_type** | **str**|  | [optional] 
 **friendly_name** | **str**|  | [optional] 

### Return type

[**List[Provider]**](Provider.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

