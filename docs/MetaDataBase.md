# MetaDataBase


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**provider_md_id** | **str** |  | 
**provider_id** | **str** |  | 
**content** | **str** |  | 
**content_type** | **str** |  | [optional] [default to 'text']
**relationships** | **List[str]** |  | [optional] [default to []]
**data** | **List[str]** |  | [optional] [default to []]
**access** | **str** |  | [optional] [default to 'public']

## Example

```python
from openapi_client.models.meta_data_base import MetaDataBase

# TODO update the JSON string below
json = "{}"
# create an instance of MetaDataBase from a JSON string
meta_data_base_instance = MetaDataBase.from_json(json)
# print the JSON string representation of the object
print(MetaDataBase.to_json())

# convert the object into a dict
meta_data_base_dict = meta_data_base_instance.to_dict()
# create an instance of MetaDataBase from a dict
meta_data_base_from_dict = MetaDataBase.from_dict(meta_data_base_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


