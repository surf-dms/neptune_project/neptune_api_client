# DataUsers


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from openapi_client.models.data_users import DataUsers

# TODO update the JSON string below
json = "{}"
# create an instance of DataUsers from a JSON string
data_users_instance = DataUsers.from_json(json)
# print the JSON string representation of the object
print(DataUsers.to_json())

# convert the object into a dict
data_users_dict = data_users_instance.to_dict()
# create an instance of DataUsers from a dict
data_users_form_dict = data_users.from_dict(data_users_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


