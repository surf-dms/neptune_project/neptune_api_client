# openapi_client.DataUsersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_data_user_data_users_post**](DataUsersApi.md#create_data_user_data_users_post) | **POST** /data_users/ | Create Data User
[**delete_data_user_data_users_delete**](DataUsersApi.md#delete_data_user_data_users_delete) | **DELETE** /data_users/ | Delete Data User
[**list_data_users_data_users_get**](DataUsersApi.md#list_data_users_data_users_get) | **GET** /data_users/ | List Data Users
[**update_data_user_data_users_id_patch**](DataUsersApi.md#update_data_user_data_users_id_patch) | **PATCH** /data_users/{id} | Update Data User


# **create_data_user_data_users_post**
> object create_data_user_data_users_post(data_user_create)

Create Data User

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.data_user_create import DataUserCreate
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DataUsersApi(api_client)
    data_user_create = openapi_client.DataUserCreate() # DataUserCreate | 

    try:
        # Create Data User
        api_response = api_instance.create_data_user_data_users_post(data_user_create)
        print("The response of DataUsersApi->create_data_user_data_users_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DataUsersApi->create_data_user_data_users_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data_user_create** | [**DataUserCreate**](DataUserCreate.md)|  | 

### Return type

**object**

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_data_user_data_users_delete**
> object delete_data_user_data_users_delete(id=id, provider_id=provider_id)

Delete Data User

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DataUsersApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str |  (optional)
    provider_id = '5eb7cf5a86d9755df3a6c593' # str |  (optional)

    try:
        # Delete Data User
        api_response = api_instance.delete_data_user_data_users_delete(id=id, provider_id=provider_id)
        print("The response of DataUsersApi->delete_data_user_data_users_delete:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DataUsersApi->delete_data_user_data_users_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | [optional] 
 **provider_id** | **str**|  | [optional] 

### Return type

**object**

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_data_users_data_users_get**
> List[DataUser] list_data_users_data_users_get(provider_id=provider_id, username=username, id=id)

List Data Users

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.data_user import DataUser
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DataUsersApi(api_client)
    provider_id = '5eb7cf5a86d9755df3a6c593' # str |  (optional)
    username = 'username_example' # str |  (optional)
    id = '5eb7cf5a86d9755df3a6c593' # str |  (optional)

    try:
        # List Data Users
        api_response = api_instance.list_data_users_data_users_get(provider_id=provider_id, username=username, id=id)
        print("The response of DataUsersApi->list_data_users_data_users_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DataUsersApi->list_data_users_data_users_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **provider_id** | **str**|  | [optional] 
 **username** | **str**|  | [optional] 
 **id** | **str**|  | [optional] 

### Return type

[**List[DataUser]**](DataUser.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_data_user_data_users_id_patch**
> DataUser update_data_user_data_users_id_patch(id, data_user_create)

Update Data User

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.data_user import DataUser
from openapi_client.models.data_user_create import DataUserCreate
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DataUsersApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str | 
    data_user_create = openapi_client.DataUserCreate() # DataUserCreate | 

    try:
        # Update Data User
        api_response = api_instance.update_data_user_data_users_id_patch(id, data_user_create)
        print("The response of DataUsersApi->update_data_user_data_users_id_patch:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DataUsersApi->update_data_user_data_users_id_patch: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **data_user_create** | [**DataUserCreate**](DataUserCreate.md)|  | 

### Return type

[**DataUser**](DataUser.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

