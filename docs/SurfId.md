# SurfId


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from openapi_client.models.surf_id import SurfId

# TODO update the JSON string below
json = "{}"
# create an instance of SurfId from a JSON string
surf_id_instance = SurfId.from_json(json)
# print the JSON string representation of the object
print(SurfId.to_json())

# convert the object into a dict
surf_id_dict = surf_id_instance.to_dict()
# create an instance of SurfId from a dict
surf_id_form_dict = surf_id.from_dict(surf_id_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


