# FriendlyName


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from openapi_client.models.friendly_name import FriendlyName

# TODO update the JSON string below
json = "{}"
# create an instance of FriendlyName from a JSON string
friendly_name_instance = FriendlyName.from_json(json)
# print the JSON string representation of the object
print(FriendlyName.to_json())

# convert the object into a dict
friendly_name_dict = friendly_name_instance.to_dict()
# create an instance of FriendlyName from a dict
friendly_name_form_dict = friendly_name.from_dict(friendly_name_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


