# openapi_client.DataApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_data_data_post**](DataApi.md#create_data_data_post) | **POST** /data/ | Create Data
[**delete_data_data_delete**](DataApi.md#delete_data_data_delete) | **DELETE** /data/ | Delete Data
[**list_data_data_get**](DataApi.md#list_data_data_get) | **GET** /data/ | List Data
[**update_data_data_id_patch**](DataApi.md#update_data_data_id_patch) | **PATCH** /data/{id} | Update Data


# **create_data_data_post**
> Data create_data_data_post(data_create)

Create Data

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.data import Data
from openapi_client.models.data_create import DataCreate
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DataApi(api_client)
    data_create = openapi_client.DataCreate() # DataCreate | 

    try:
        # Create Data
        api_response = api_instance.create_data_data_post(data_create)
        print("The response of DataApi->create_data_data_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DataApi->create_data_data_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data_create** | [**DataCreate**](DataCreate.md)|  | 

### Return type

[**Data**](Data.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_data_data_delete**
> object delete_data_data_delete(id=id)

Delete Data

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DataApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str |  (optional)

    try:
        # Delete Data
        api_response = api_instance.delete_data_data_delete(id=id)
        print("The response of DataApi->delete_data_data_delete:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DataApi->delete_data_data_delete: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | [optional] 

### Return type

**object**

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_data_data_get**
> List[Data] list_data_data_get(email=email, key_in_provider=key_in_provider, provider_id=provider_id, usernames=usernames)

List Data

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.data import Data
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DataApi(api_client)
    email = 'email_example' # str |  (optional)
    key_in_provider = 'key_in_provider_example' # str |  (optional)
    provider_id = '5eb7cf5a86d9755df3a6c593' # str |  (optional)
    usernames = ['usernames_example'] # List[Optional[str]] |  (optional)

    try:
        # List Data
        api_response = api_instance.list_data_data_get(email=email, key_in_provider=key_in_provider, provider_id=provider_id, usernames=usernames)
        print("The response of DataApi->list_data_data_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DataApi->list_data_data_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **str**|  | [optional] 
 **key_in_provider** | **str**|  | [optional] 
 **provider_id** | **str**|  | [optional] 
 **usernames** | [**List[Optional[str]]**](str.md)|  | [optional] 

### Return type

[**List[Data]**](Data.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_data_data_id_patch**
> Data update_data_data_id_patch(id, data_create)

Update Data

### Example

* Basic Authentication (HTTPBasic):

```python
import openapi_client
from openapi_client.models.data import Data
from openapi_client.models.data_create import DataCreate
from openapi_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost
# See configuration.py for a list of all supported configuration parameters.
configuration = openapi_client.Configuration(
    host = "http://localhost"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: HTTPBasic
configuration = openapi_client.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with openapi_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = openapi_client.DataApi(api_client)
    id = '5eb7cf5a86d9755df3a6c593' # str | 
    data_create = openapi_client.DataCreate() # DataCreate | 

    try:
        # Update Data
        api_response = api_instance.update_data_data_id_patch(id, data_create)
        print("The response of DataApi->update_data_data_id_patch:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DataApi->update_data_data_id_patch: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **str**|  | 
 **data_create** | [**DataCreate**](DataCreate.md)|  | 

### Return type

[**Data**](Data.md)

### Authorization

[HTTPBasic](../README.md#HTTPBasic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Successful Response |  -  |
**404** | Not found |  -  |
**422** | Validation Error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

