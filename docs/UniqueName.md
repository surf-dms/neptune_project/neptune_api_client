# UniqueName


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

## Example

```python
from openapi_client.models.unique_name import UniqueName

# TODO update the JSON string below
json = "{}"
# create an instance of UniqueName from a JSON string
unique_name_instance = UniqueName.from_json(json)
# print the JSON string representation of the object
print(UniqueName.to_json())

# convert the object into a dict
unique_name_dict = unique_name_instance.to_dict()
# create an instance of UniqueName from a dict
unique_name_form_dict = unique_name.from_dict(unique_name_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


